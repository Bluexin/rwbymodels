package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.AmmoCapDTO;
import be.bluexin.rwbym.weaponry.dto.AmmoDTO;
import be.bluexin.rwbym.weaponry.dto.RecipeDTO;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
@MethodsReturnNonnullByDefault
@ParametersAreNonnullByDefault
public class RWBYAmmoItem extends Item implements ICustomItem {

    private final RecipeDTO[] recipes;
    private final AmmoCapDTO capabilities;
    //public int getAmmoMax;

    public RWBYAmmoItem(AmmoDTO from) {
        this.setCreativeTab(RWBYModels.tab_rwbyweapons);
        this.setRegistryName(new ResourceLocation(RWBYModels.MODID, from.getName()));
        this.setUnlocalizedName(this.getRegistryName().toString());
        this.recipes = from.getRecipes();
        this.capabilities = from.getEntityCapabilities();
        this.setMaxStackSize(from.getAmmoMax());
        //this.getAmmoMax = from.getAmmoMax();
    }

    //@SideOnly(Side.CLIENT)
    //public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items)
    //{
    //if(isInCreativeTab(tab))
    //{
    //Add empty and full clips
    //ItemStack clip = new ItemStack(this);
    //setBulletsAmount(clip, 0);
    //items.add(clip.copy());
    //setBulletsAmount(clip, getAmmoMax);
    //items.add(clip);
    //}
    //}

    //public static void setBulletsAmount(ItemStack stack, int amount)
    //{
    //NBTHelper.setInteger(stack, "ammo", Math.max(Math.min(amount, ((RWBYAmmoItem) stack.getItem()).getAmmoMax), 0));
    //}

    //public static int getBulletsAmount(ItemStack stack)
    //{
    //return NBTHelper.getInt(stack, "ammo");
    //}

    //@SideOnly(Side.CLIENT)
    //public void addInformation(ItemStack stack, @Nullable World world, List<String> tooltip)
    //{
    //tooltip.add("Bullets: " + getBulletsAmount(stack) + "/" + getAmmoMax);
    //}

    //public int getMaxAmmo()
    //{
    //return getAmmoMax;
    //}




    
    public EntityArrow createArrow(World worldIn, ItemStack stack, EntityLivingBase shooter) {
        return new RWBYAmmoEntity(worldIn, shooter, this.capabilities, this);
    }

    public boolean isInfinite(@Nullable ItemStack stack, @Nullable ItemStack bow, @Nullable EntityPlayer player) {
        return capabilities.isInfinite() || (bow != null && EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, bow) > 0);
    }

    @Override
    public void registerRecipes() {
        if (this.recipes != null) for (RecipeDTO recipe : this.recipes) {
            recipe.register(this);
        }
    }

    @Override
    public String toString() {
        return "RWBYAmmoItem{" + this.getRegistryName() + "}";
    }
}
