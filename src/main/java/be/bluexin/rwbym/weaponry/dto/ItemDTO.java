package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym by Bluexin.
 * <p>
 * Refers only to non-equipment type items (crafting mats)
 *
 * @author Bluexin
 */
public class ItemDTO {
    private final String name;
    private final RecipeDTO[] recipes;

    public ItemDTO(String name, RecipeDTO[] recipe) {
        this.name = name;
        this.recipes = recipe;
    }

    public String getName() {
        return name;
    }

    public RecipeDTO[] getRecipes() {
        return recipes;
    }
}
