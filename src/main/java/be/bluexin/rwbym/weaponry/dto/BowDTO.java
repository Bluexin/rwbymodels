package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class BowDTO {
    private final String name;
    private final int durability;
    private final int drawSpeed;
    private final String data;
    private final RecipeDTO[] recipes;
    private final int enchantability;
    private final String morph;
    private final String ammo;
    private final boolean noCharge;
    private final float projectileSpeed;

    public BowDTO(String name, int durability, int drawSpeed, int enchantability, String data, RecipeDTO[] recipes, String morph, String ammo, boolean noCharge, float projectileSpeed, boolean usesAmmo) {
        this.name = name;
        this.durability = durability;
        this.drawSpeed = drawSpeed;
        this.data = data;
        this.recipes = recipes;
        this.enchantability = enchantability;
        this.morph = morph;
        this.ammo = ammo;
        this.noCharge = noCharge;
        this.projectileSpeed = projectileSpeed;
    }

    public String getName() {
        return name;
    }

    public int getDurability() {
        return durability;
    }

    public int getDrawSpeed() {
        return drawSpeed;
    }

    public String getData() {
        return data;
    }

    public RecipeDTO[] getRecipes() {
        return recipes;
    }

    public int getEnchantability() {
        return enchantability;
    }

    public String getMorph() {
        return morph;
    }

    public String getAmmo() {
        return ammo;
    }

    public boolean isCharging() {
        return !noCharge;
    }

    public float getProjectileSpeed() {
        return projectileSpeed;
    }
}
