package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
public class AmmoDTO {
    private final String name;
    private final RecipeDTO[] recipes;
    private final AmmoCapDTO entityCapabilities;
    private final int ammoMax;

    public AmmoDTO(String name, RecipeDTO[] recipes, int ammoMax, AmmoCapDTO entityCapabilities) {
        this.name = name;
        this.recipes = recipes;
        this.ammoMax = ammoMax;
        this.entityCapabilities = entityCapabilities;
    }

    public String getName() {
        return name;
    }

    public int getAmmoMax() {
        return ammoMax;
    }

    public RecipeDTO[] getRecipes() {
        return recipes;
    }

    public AmmoCapDTO getEntityCapabilities() {
        return entityCapabilities;
    }
}
