package be.bluexin.rwbym.weaponry.dto;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class SwordDTO {
    private final String name;
    private final int durability;
    private final float damage;
    private final int enchantability;
    private final String data;
    private final RecipeDTO[] recipes;
    private final String morph;
    private final boolean shield;

    public SwordDTO(String name, int durability, float damage, int enchantability, String data, RecipeDTO[] recipes, String morph, boolean shield) {
        this.name = name;
        this.durability = durability;
        this.damage = damage;
        this.enchantability = enchantability;
        this.data = data;
        this.recipes = recipes;
        this.morph = morph;
        this.shield = shield;
    }

    public String getName() {
        return name;
    }

    public int getDurability() {
        return durability;
    }

    public float getDamage() {
        return damage;
    }

    public int getEnchantability() {
        return enchantability;
    }

    public String getData() {
        return data;
    }

    public RecipeDTO[] getRecipes() {
        return recipes;
    }

    public String getMorph() {
        return morph;
    }

    public boolean isShield() {
        return shield;
    }
}
