package be.bluexin.rwbym.weaponry.dto;

import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
public class AmmoCapDTO {
    private final boolean canPickup;
    private final String texture;
    private final double baseDamage;
    private final boolean gravity;
    private final boolean infinite;
    private final String nbt;
    private transient NBTTagCompound nbtTag;

    public AmmoCapDTO(boolean canPickup, String texture, double baseDamage, boolean gravity, boolean infinite, String nbt) {
        this.canPickup = canPickup;
        this.texture = texture;
        this.baseDamage = baseDamage;
        this.gravity = gravity;
        this.infinite = infinite;
        this.nbt = nbt;
        if (nbt != null) try {
            this.nbtTag = JsonToNBT.getTagFromJson(nbt);
        } catch (NBTException e) {
            e.printStackTrace();
            System.err.println("Invalid NBT !");
        }
    }

    public boolean canPickup() {
        return canPickup;
    }

    public String getTexture() {
        return texture;
    }

    public double getBaseDamage() {
        return baseDamage;
    }

    public boolean obeysGravity() {
        return gravity;
    }

    public boolean isInfinite() {
        return infinite;
    }

    public NBTTagCompound getNbt() {
        if (nbtTag == null && nbt != null) try {
            this.nbtTag = JsonToNBT.getTagFromJson(nbt);
        } catch (NBTException e) {
            e.printStackTrace();
            System.err.println("Invalid NBT !");
        }
        return nbtTag;
    }
}
