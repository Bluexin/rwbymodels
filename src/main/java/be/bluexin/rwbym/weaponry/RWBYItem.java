package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.ItemDTO;
import be.bluexin.rwbym.weaponry.dto.RecipeDTO;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
public class RWBYItem extends Item implements ICustomItem {

    private final RecipeDTO[] recipes;

    public RWBYItem(ItemDTO from) {
        this.setRegistryName(new ResourceLocation(RWBYModels.MODID, from.getName()));
        this.setUnlocalizedName(this.getRegistryName().toString());
        this.recipes = from.getRecipes();
        this.setCreativeTab(RWBYModels.tab_rwbyitems);
    }

    @Override
    public void registerRecipes() {
        if (this.recipes != null) for (RecipeDTO recipe : this.recipes) {
            recipe.register(this);
        }
    }

    @Override
    public String toString() {
        return "RWBYItem{" + this.getRegistryName() + "}";
    }
}
