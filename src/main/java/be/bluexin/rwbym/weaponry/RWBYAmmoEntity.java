package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.AmmoCapDTO;
import mcp.MethodsReturnNonnullByDefault;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;

/**
 * Part of rwbym by Bluexin.
 *
 * @author Bluexin
 */
@MethodsReturnNonnullByDefault
public class RWBYAmmoEntity extends EntityArrow { // FIXME: for some reason these are shaking like a superpowered vibrator
    private static final DataParameter<String> RS = EntityDataManager.createKey(RWBYAmmoEntity.class, DataSerializers.STRING);

    private ResourceLocation rs = new ResourceLocation("minecraft", "textures/entity/projectiles/spectral_arrow.png");
    private RWBYAmmoItem itemRef;

    private NBTTagCompound nbt = null;

    private static final MethodHandle getTicksInGround;

    private double maxMotionX = 0D;
    private double maxMotionY = 0D;
    private double maxMotionZ = 0D;


    public RWBYAmmoEntity(World world) {
        super(world);
        this.dataManager.set(RS, this.rs.toString());
    }

    static {
        try {
            Field f = ReflectionHelper.findField(EntityArrow.class, "ticksInGround", "field_70252_j");
            f.setAccessible(true);
            getTicksInGround = MethodHandles.publicLookup().unreflectGetter(f);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public RWBYAmmoEntity(World worldIn, double x, double y, double z, AmmoCapDTO capabilities, RWBYAmmoItem from) {
        super(worldIn, x, y, z);
        this.rs = new ResourceLocation(RWBYModels.MODID, capabilities.getTexture());
        this.dataManager.set(RS, this.rs.toString());
        this.setDamage(capabilities.getBaseDamage());
        this.setNoGravity(!capabilities.obeysGravity());
        this.itemRef = from;

        if (capabilities.getNbt() != null && !world.isRemote) this.nbt = capabilities.getNbt();
    } // TODO: add to dispenser registries (see usage of this constructor in other impl of EntityArrow

    public RWBYAmmoEntity(World worldIn, EntityLivingBase shooter, AmmoCapDTO capabilities, RWBYAmmoItem from) {
        this(worldIn, shooter.posX, shooter.posY + (double) shooter.getEyeHeight() - 0.10000000149011612D, shooter.posZ, capabilities, from);
        this.shootingEntity = shooter;

        if (shooter instanceof EntityPlayer && capabilities.canPickup()) this.pickupStatus = PickupStatus.ALLOWED;
    }

    protected void entityInit() {
        super.entityInit();
        this.dataManager.register(RS, "");
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(itemRef);
    }

    ResourceLocation getRs() {
        return new ResourceLocation(this.dataManager.get(RS));
    }

    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);

        compound.setString("itemRef", this.itemRef == null ? "" : this.itemRef.getRegistryName().toString());
        compound.setString("texture", this.rs.toString());
    }

    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);

        if (compound.hasKey("itemRef"))
            this.itemRef = (RWBYAmmoItem) Item.getByNameOrId(compound.getString("itemRef"));
        if (compound.hasKey("texture")) {
            String s = compound.getString("texture");
            this.rs = s.length() > 0 ? new ResourceLocation(s) : null;
            if (this.rs != null) this.dataManager.set(RS, this.rs.toString());
        }
    }

    @Override
    public void setDead() {
        this.getPassengers().forEach(it -> {
            it.motionX = this.maxMotionX;
            it.motionY = this.maxMotionY;
            it.motionZ = this.maxMotionZ;
        });
        super.setDead();
    }

    @Override
    public void onUpdate() {
        super.onUpdate();

        if (this.nbt != null) {
            this.maxMotionX = this.motionX;
            this.maxMotionY = this.motionY;
            this.maxMotionZ = this.motionZ;

            if (nbt.hasKey("Passengers", 9)) {
                NBTTagList nbttaglist = nbt.getTagList("Passengers", 10);

                for (int i = 0; i < nbttaglist.tagCount(); ++i) {
                    Entity entity = AnvilChunkLoader.readWorldEntityPos(nbttaglist.getCompoundTagAt(i), world, posX, posY, posZ, false);
                    if (entity != null) {
                        entity.isDead = false;
                        world.spawnEntity(entity);
                        entity.startRiding(this, true);
                    }
                }
            }
            this.nbt = null;
        }

        if (this.inGround && getTicksInGround() > 1) this.setDead();
    }

    private int getTicksInGround() {
        try {
            return (Integer) getTicksInGround.invoke(this);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return 0;
        }
    }
}
