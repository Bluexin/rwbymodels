package be.bluexin.rwbym.weaponry;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.dto.CollectionDTO;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class RWBYLoader {
    public static List<ICustomItem> load() {
        try (InputStreamReader isr = new InputStreamReader(RWBYLoader.class.getClassLoader().getResourceAsStream("assets/rwbym/collection.rwbym"))) {
            CollectionDTO c = new Gson().fromJson(isr, CollectionDTO.class);
            List<ICustomItem> l = new ArrayList<>(c.getBows().size() + c.getSwords().size());
            c.getSwords().forEach(s -> l.add(new RWBYSword(s)));
            c.getBows().forEach(b -> l.add(new RWBYBow(b)));
            c.getItems().forEach(i -> l.add(new RWBYItem(i)));
            c.getAmmo().forEach(a -> l.add(new RWBYAmmoItem(a)));
            return l;
        } catch (Exception e) {
            LogManager.getLogger(RWBYModels.MODID).error("An error occurred while loading RWBY Models weapons definitions file.");
        }

        return new ArrayList<>();
    }
}
