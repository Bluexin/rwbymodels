package be.bluexin.rwbym;

import org.lwjgl.input.Keyboard;

/**
 * Created by Konata on 8/1/2017.
 */
public enum RWBYKeybind {

    EXTRA_ACTION("rwby.key.extra_action", Keyboard.KEY_R),
    CHARACTER_SCREEN("rwby.key.character_screen", Keyboard.KEY_C);

    public final String keyName;
    public final int defaultKeyCode;

    RWBYKeybind(String keyName, int defaultKeyCode) {
        this.keyName = keyName;
        this.defaultKeyCode = defaultKeyCode;
    }
}
