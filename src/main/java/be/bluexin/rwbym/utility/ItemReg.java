package be.bluexin.rwbym.utility;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.weaponry.RWBYIconItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.FMLLaunchHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemReg {

    public static Item Icon;

    @SideOnly(Side.CLIENT)
    public static void registerItemRenderer(Item item) {
        RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
        renderItem.getItemModelMesher().register(item, 0, new ModelResourceLocation(RWBYModels.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }


    public static void ItemReg() {
        Icon = addItem(new RWBYIconItem(), "Icon");
    }

    private static Item addItem(Item item, String name) {

        item.setUnlocalizedName(name);
        item.setRegistryName(name);
        ForgeRegistries.ITEMS.register(item);
        if (FMLLaunchHandler.side() == Side.CLIENT) {
            registerItemRenderer(item);
        }
        return item;

    }

}
