package be.bluexin.rwbym;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
//taking a long time cause ofviwer

/**
 * Created by Blaez on 7/31/2017.
 */
@SideOnly(Side.CLIENT)
public class RWBYKeybinding {

    public static RWBYKeybinding INSTANCE = new RWBYKeybinding();
    private KeyBinding[] keyBindings = null;

    private RWBYKeybinding() {
        keyBindings = new KeyBinding[1]; // TODO: add all RWBYKeybind
        keyBindings[0] = new KeyBinding("key.structure.desc", Keyboard.KEY_Z, "key.rwbym.category");
    }

    public void registerKeyBinds() {
        for (KeyBinding keyBinding : keyBindings) ClientRegistry.registerKeyBinding(keyBinding);
    }

    @SubscribeEvent
    public void onEvent(InputEvent.KeyInputEvent event) {
        System.out.println("Key Input Event");

        if (keyBindings[0].isPressed()) {
            System.out.println("Key binding =" + keyBindings[0].getKeyDescription());
        }
    }
}
