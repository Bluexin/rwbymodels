package be.bluexin.rwbym.proxy;

import be.bluexin.rwbym.RWBYModels;
import be.bluexin.rwbym.entity.*;
import be.bluexin.rwbym.entity.renderer.*;
import be.bluexin.rwbym.weaponry.ICustomItem;
import be.bluexin.rwbym.weaponry.RWBYAmmoEntity;
import be.bluexin.rwbym.weaponry.RWBYAmmoRender;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

/**
 * Part of rwbym
 *
 * @author Bluexin
 */
public class ClientProxy extends CommonProxy {

    @Override
    public void registerRenderers(RWBYModels ins) {

    }

    public void preInit() {
        super.preInit();
        RenderingRegistry.registerEntityRenderingHandler(RWBYAmmoEntity.class, RWBYAmmoRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityBeowolf.class, BeowolfRender.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityUrsa.class, UrsaRender.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityBoarbatusk.class, BoarbatuskRender.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntitySmallFireball.class, FireballRender.FACTORY);
        RenderingRegistry.registerEntityRenderingHandler(EntityLargeFireball.class, LargeFireballRender.FACTORY);
//        MinecraftForge.EVENT_BUS.register(RWBYKeybinding.INSTANCE);
//        
    }

    public void init() {
        super.init();
        if (RWBYModels.items != null) RWBYModels.items.forEach(ICustomItem::registerModel);
    }
}
