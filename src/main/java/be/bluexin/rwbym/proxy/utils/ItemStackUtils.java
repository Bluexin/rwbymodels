package be.bluexin.rwbym.proxy.utils;

import net.minecraft.item.ItemStack;

/**
 * Leaving this class here for future reference
 * A.C.
 */
public class ItemStackUtils {
    public static int getStackSize(ItemStack stack) {
        if (stack.isEmpty()) {
            return 0;
        } else {
            return stack.getCount();
        }
    }
}
