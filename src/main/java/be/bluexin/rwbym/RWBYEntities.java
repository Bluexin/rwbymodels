package be.bluexin.rwbym;

import be.bluexin.rwbym.entity.*;
import be.bluexin.rwbym.weaponry.RWBYAmmoEntity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;

import java.util.ArrayList;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class RWBYEntities {

    public static Object instance;
    public int mobid = 0;

    public static Biome[] clean(net.minecraft.util.registry.RegistryNamespaced<ResourceLocation, Biome> in) {
        Iterator<Biome> itr = in.iterator();
        ArrayList<Biome> ls = new ArrayList<Biome>();
        while (itr.hasNext()) {
            ls.add(itr.next());
        }
        return ls.toArray(new Biome[ls.size()]);
    }

    public void preInit(FMLPreInitializationEvent event) {
        int entityID = 333000;
        mobid = entityID;

        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:beowolf"), EntityBeowolf.class, "beowolf", entityID, instance, 64, 1, true, (0 << 16) + (0 << 8) + 0, (153 << 16) + (153 << 8) + 153);
        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:projectiles"), RWBYAmmoEntity.class, "rwbyprojectile", entityID + 1, instance, 64, 1, false);
        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:ursa"), EntityUrsa.class, "ursa", entityID + 2, instance, 64, 1, true, (0 << 16) + (0 << 8) + 0, (153 << 16) + (153 << 8) + 153);
        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:boarbatusk"), EntityBoarbatusk.class, "boarbatusk", entityID + 3, instance, 64, 1, true, (0 << 16) + (0 << 8) + 0, (153 << 16) + (153 << 8) + 153);
        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:fireball"), EntitySmallFireball.class, "fireball", entityID + 4, instance, 64, 1, true);
        EntityRegistry.registerModEntity(new ResourceLocation("rwbym:largefireball"), EntityLargeFireball.class, "largeFireball", entityID + 5, instance, 64, 1, true);

        EntityRegistry.addSpawn(EntityBeowolf.class, 100, 3, 30, EnumCreatureType.MONSTER, clean(Biome.REGISTRY));
        EntityRegistry.addSpawn(EntityUrsa.class, 100, 3, 30, EnumCreatureType.MONSTER, clean(Biome.REGISTRY));
        EntityRegistry.addSpawn(EntityBoarbatusk.class, 100, 3, 30, EnumCreatureType.MONSTER, clean(Biome.REGISTRY));

        DungeonHooks.addDungeonMob(new ResourceLocation("rwbym:beowolf"), 180);
        DungeonHooks.addDungeonMob(new ResourceLocation("rwbym:ursa"), 180);
        DungeonHooks.addDungeonMob(new ResourceLocation("rwbym:boarbatusk"), 180);

    }

}
